function [ select,mind_ ] = match( d , thr, pen1, pen2)
%MATCH	based on distance computed with dist.m until "thr" pairs are found that best match
% pen1 and pen2 are optional penalitied 
if (nargin > 2) && ~isempty(pen1)
    d = bsxfun(@plus,d,pen1(:));
end


if (nargin > 3) && ~isempty(pen2)
    d = bsxfun(@plus,d,pen2(:).');
end

select = []; mind = 0; i = 0;
while i < thr
    i = i+1;
    [mind,pos] = nanmin(d(:));
    if isnan(mind)
        break
    end
    [posx,posy] = ind2sub(size(d),pos);
    d(posx,:) = NaN;
    d(:,posy) = NaN;
    mind_(i) = mind;
    select(i,:) = [posx,posy];
end

fprintf('Matching %d x %d, found %d pairs\n',size(d,1),size(d,2),length(mind_));

end

