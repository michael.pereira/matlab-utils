function [ tperm_min,tperm_max] = permstatnull2( var1,var2,nrand,pclust,testtype,output)
%permstatnull2 Estimates the null distribution of differences between var1 and
%var2
%   Input variables:
%       * var1,var2: the two variables to be compared (should be NxK1xK2,
%       where N is the number of subjects or trials and K2 could be 1)
%       * nrand: the number of permutations (default: 1000)
%       * pclust: the p-value threshold for clustering
%       * testtype: whether to apply paired (ttest) or two-sample (ttest2)
%       * output: set to 0 to remove anoying progress bar
%   Output variables:
%       * tmin: the null distribution of minimum values
%       * tmax: the null distribution of maximum values
if (nargin < 6)
    output = 1;
end
if (nargin < 5) || isempty(testtype);
    testtype = 'ttest';
end
if (nargin < 4 ) || isempty(pclust)
    pclust = 0.05;
end
if (nargin < 3 ) || isempty(nrand)
    nrand = 1000;
end

[nsamp1,n1a,n1b] = size(var1);
[nsamp2,n2a,n2b] = size(var2);

if (n1a ~= n2a) || (n1b ~= n2b)
    error('2nd and 3rd dimension should be identical between the two variables');
end
if (nsamp1 ~= nsamp2) && strcmp(testtype,'ttest')
    error('When using paired ttest, you need the same number of trials/subjects in each condition');
end

if output
    fprintf('BOOTSTRAP: #c1=%d, #c2=%d - n= [%dx%d] - repeat: %d)',nsamp1,nsamp2,n1a,n1b,nrand);
    fprintf('completed: |          |');%fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
end
tperm_min = zeros(1,nrand);
tperm_max = zeros(1,nrand);

for r=1:nrand
    % smart-ass fancy output
    if (mod(r,nrand/10)==1) && output
        rperc = ceil(r/nrand*10);
        fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
        fprintf('|');
        for j=1:10
            if j<=rperc
                fprintf('=');
            else
                fprintf(' ');
            end
        end
        fprintf('|');
    end
    
    
   
    if strcmp(testtype,'ttest')
        rnd = rand(1,nsamp1) > 0.5;
        var1p = bsxfun(@times,var1,rnd.') + bsxfun(@times,var2,~rnd.');
        var2p = bsxfun(@times,var1,~rnd.') + bsxfun(@times,var2,rnd.');
        % paired ttest
        [~,p_,~,stat_] = ttest(var1p,var2p);
    elseif strcmp(testtype,'ttest2')
        perm = randperm(nsamp1+nsamp2);
        alldata = [var1 ; var2];
        lab = [zeros(1,nsamp1) ones(1,nsamp2)];
        lab = lab(perm);
        % two-sample ttest
        [~,p_,~,stat_] = ttest2(alldata(lab==0,:,:),alldata(lab==1,:,:));
    else
        error('Test type can only be ttest or ttest2, or you implement it');
    end
        
    tpermsum = permstatclust( squeeze(stat_.tstat),squeeze(p_),pclust );
    tperm_min(r) = 0;%min(stat_.tstat);
    tperm_max(r) = 0;%max(stat_.tstat);
    tmin = min(tpermsum(tpermsum < 0));
    tmax = max(tpermsum(tpermsum > 0));
    if ~isempty(tmin)
        tperm_min(r) = tmin;
    end
    if ~isempty(tmax)
        tperm_max(r) = tmax;
    end
    
    
end
if output
    fprintf('\n');
end
end

