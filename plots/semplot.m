function semplot(varargin )
%SEMPLOT Plot mean and standard error of the mean
% Michael Pereira <michael.pereira@epfl.ch>
if (length(varargin)==1) || ~isnumeric(varargin{2})
    va = 2;
    data = varargin{1};
    x = 1:size(data,2);
    sem = nanstd(data)./sqrt(size(data,1));
elseif (length(varargin)==2) || ~isnumeric(varargin{3})
    va = 3;
    x = varargin{1};
    data = varargin{2};
    sem = nanstd(data)./sqrt(size(data,1));
else
    va = 4;
    x = varargin{1};
    data = varargin{2};
    sem = varargin{3};
end
hold on;

plot(x,nanmean(data),varargin{va:end},'LineWidth',2);
plot(x,nanmean(data)+sem,varargin{va:end});
plot(x,nanmean(data)-sem,varargin{va:end});
%plot(x,data,varargin{:});

end

