function [ out ] = outliers( data, w )
%
%OUTLIERS Simple function to remove outliers based on IQR
%
% Michael Pereira <michael.pereira@epfl.ch>
if nargin == 1
    w = 1.5;
end

q1 = quantile(data,0.25);
q3 = quantile(data,0.75);
out = (data > q3 + w*(q3-q1)) | (data < q1 - w*(q3-q1));
end

